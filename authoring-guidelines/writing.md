

<<TableOfContents(3)>>

# Overview

This is document is a thorough overview of what writing an extension entails. It describes the anatomy of a typical extension, how to build one from scratch, general debugging functions and logging, as well as some logistics like version compatibility.

It does not include instructions for modifying specific aspects of GNOME Shell, general JavaScript programming, GNOME API usage or other topics that are explained elsewhere.

Below are some links that may be helpful when developing a GNOME Shell extension:

 * **GJS & JavaScript**
   * [GJS Documentation](https://gitlab.gnome.org/GNOME/gjs/blob/master/doc/Home.md)
   * [GJS GObject Usage](https://gitlab.gnome.org/GNOME/gjs/blob/master/doc/Mapping.md)
   * [GJS Built-in Modules](https://gitlab.gnome.org/GNOME/gjs/blob/master/doc/Modules.md)
   * [GNOME API Documentation](https://gjs-docs.gnome.org/)
   * [MDN JavaScript Reference](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference)
 * **GNOME Shell**
   * [GNOME Shell JavaScript source](https://gitlab.gnome.org/GNOME/gnome-shell/tree/master/js/)
   * [ExtensionUtils Module](https://gitlab.gnome.org/GNOME/gnome-shell/tree/master/js/misc/extensionUtils.js)
   * [LookingGlass](https://wiki.gnome.org/Projects/GnomeShell/LookingGlass)
   * [Shell Documentation](https://gjs-docs.gnome.org/shell01~0.1_api/)
   * [St Documentation](https://gjs-docs.gnome.org/st10~1.0_api/)
   * [Clutter Documentation](https://gjs-docs.gnome.org/#q=clutter4)
   * [Mutter Documentation](https://gjs-docs.gnome.org/#q=meta4)
 * **Getting Help**
   * Ask on [discourse](https://discourse.gnome.org/) using the [extensions](https://discourse.gnome.org/tag/extensions) tag
   * Ask on StackOverflow using the [gnome-shell-extensions](https://stackoverflow.com/questions/tagged/gnome-shell-extensions) and/or [gjs](https://stackoverflow.com/questions/tagged/gjs) tags
 * Ask in the extensions IRC/Matrix room
  * irc://irc.gimpnet.org/shell-extensions
  * https://matrix.to/#/#extensions:gnome.org

## What is an extension?

GNOME Shell's UI and extensions are written in GJS, which is JavaScript bindings for the GNOME C APIs. This includes libraries like Gtk, GLib/Gio, Clutter, GStreamer and many others. Just like how PyGObject is Python bindings for the same libraries.

JavaScript is a prototype-based language, which for us means that we can modify the UI and behaviour of GNOME Shell *while* it is running. This is what is known as "monkey-patching". For example, you could override the [addMenuItem()](https://gitlab.gnome.org/GNOME/gnome-shell/blob/gnome-3-30/js/ui/popupMenu.js#L649) function of the `PopupMenu` class and all existing or newly created `PopupMenu` classes and subclasses will immediately start using your override.

GNOME Shell extensions are effectively patches that are applied to GNOME Shell when they are enabled, and reverted when they are disabled. In fact, the only real difference between patches merged into GNOME Shell and an extension is when the patch is applied.

## What makes an extension?

Whether you're downloading from a git repository (eg. GitHub, GitLab) or installing from https://extensions.gnome.org, extensions are distributed as Zip files with only two required files: `metadata.json` and `extension.js`. A more complete, zipped extension usually looks like this:

```sh
example@wiki.gnome.org.zip
    locale/
        de/
          LC_MESSAGES/
              example.mo
    schemas/
        gschemas.compiled
        org.gnome.shell.extensions.example.gschema.xml
    extension.js
    metadata.json
    prefs.js
    stylesheet.css
```

Once unpacked and installed the extension will be in one of two places:

```sh
// User Extension
~/.local/share/gnome-shell/extensions/example@wiki.gnome.org/
    extension.js
    metadata.json
    ...

// System Extension
/usr/share/gnome-shell/extensions/example@wiki.gnome.org/
    extension.js
    metadata.json
    ...
```

The topic of `schemas/` (GSettings) is explained below in [Extension Preferences](#Extension-Preferences) and `locale/` (Gettext translations) in [Extension Translations](#Extension_Translations).


### metadata.json (Required)

`metadata.json` is a required file of every extension. It contains basic information about the extension including its name, a description, version and a few other things. Below is a complete example:

```js
{
    "uuid": "example@wiki.gnome.org",
    "name": "Example",
    "description": "This extension puts an icon in the panel with a simple dropdown menu.",
    "version": 1,
    "shell-version": [ "3.30", "3.32" ],
    "url": "https://gitlab.gnome.org/World/ShellExtensions/gnome-shell-extension-example"
}
```

These fields should be pretty self-explanatory, with some simple rules:

#### uuid

`uuid` is a globally-unique identifier for your extension, made of two parts separated by `@`. An extension's files must be installed to a folder with the same name as `uuid`:

```sh
~/.local/share/gnome-shell/extensions/example@wiki.gnome.org/
```

The first part should be a simple string (possibly a variation of the extension name) like "click-to-focus" and the second part should be some namespace under your control such as `username.github.io`. Common examples are `myextension@account.gmail.com` and `my-extension@username.github.io`.

#### name

`name` should be a short, descriptive string like "Click To Focus", "Adblock" or "Shell Window Shrinker".

#### description

`description` should be a relatively short description of the extension's function. If you need to, you can insert line breaks and tabs by using the `\n` and `\t` escape sequences.

#### shell-version

`shell-version` is an array of GNOME Shell versions your extension supports and must include at least one version.

#### url

`url` is required for extensions submitted to https://extensions.gnome.org/ and usually points to a Github or GitLab repository. It should at least refer to a website where users can report issues and get help using the extension.

#### version

`version` is the version of your extension and should be a whole number like `1`, **not** a semantic version like `1.1` or a string like `"1"`.

#### settings-schema & gettext-domain

These two fields are optional and are use by the `ExtensionUtils` module which has two helper functions for initializing GSettings and Gettext translations. `settings-schema` should be a GSchema Id like `org.gnome.shell.extensions.example` and `gettext-domain` should be a unique domain for your extension's translations. You could use the same domain as your GSchema Id or the UUID of your extension like `example@wiki.gnome.org`.


### extension.js (Required)

`extension.js` is a required file of every extension. It is the core of your extension and contains the function hooks `init()`, `enable()` and `disable()` used by GNOME Shell to load, enable and disable your extension.

```js
// Always have this as the first line of your file. Google for an explanation.
'use strict';

// This is a handy import we'll use to grab our extension's object
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

// Like `init()` below, code *here* in the top-level of your script is executed
// when your extension is loaded. You MUST NOT make any changes to GNOME Shell
// here and typically you should do nothing but assign variables.
const SOME_CONSTANT = 42;


// This function is called once when your extension is loaded, not enabled. This
// is a good time to setup translations or anything else you only do once.
//
// You MUST NOT make any changes to GNOME Shell, connect any signals or add any
// MainLoop sources here.
function init() {
    log(`initializing ${Me.metadata.name} version ${Me.metadata.version}`);
}

// This function could be called after your extension is enabled, which could
// be done from GNOME Tweaks, when you log in or when the screen is unlocked.
//
// This is when you setup any UI for your extension, change existing widgets,
// connect signals or modify GNOME Shell's behaviour.
function enable() {
    log(`enabling ${Me.metadata.name} version ${Me.metadata.version}`);
}

// This function could be called after your extension is uninstalled, disabled
// in GNOME Tweaks, when you log out or when the screen locks.
//
// Anything you created, modifed or setup in enable() MUST be undone here. Not
// doing so is the most common reason extensions are rejected during review!
function disable() {
    log(`disabling ${Me.metadata.name} version ${Me.metadata.version}`);
}
```

### prefs.js

`prefs.js` is used to build a Gtk widget that will be inserted into a window and be used as the preferences dialog for your extension. If this file is not present, there will simply be no preferences button in GNOME Tweaks or on https://extensions.gnome.org/local/.

```js
'use strict';

const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;

// It's common practice to keep GNOME API and JS imports in separate blocks
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();


// Like `extension.js` this is used for any one-time setup like translations.
function init() {
    log(`initializing ${Me.metadata.name} Preferences`);
}


// This function is called when the preferences window is first created to build
// and return a Gtk widget. As an example we'll create and return a GtkLabel.
function buildPrefsWidget() {
    // This could be any GtkWidget subclass, although usually you would choose
    // something like a GtkGrid, GtkBox or GtkNotebook
    let prefsWidget = new Gtk.Label({
        label: `${Me.metadata.name} version ${Me.metadata.version}`,
        visible: true
    });

    // At the time buildPrefsWidget() is called, the window is not yet prepared
    // so if you want to access the headerbar you need to use a small trick
    GLib.timeout_add(0, () => {
        let window = prefsWidget.get_toplevel();
        let headerBar = window.get_titlebar();
        headerbar.title = `${Me.metadata.name} Preferences`;
    });

    return prefsWidget;
}
```

Something that's important to understand:

 * The code in `extension.js` is executed in the same process as `gnome-shell`
 * The code in `prefs.js` will be executed in a separate Gtk process

In `extension.js` you **will** have access to live code running in GNOME Shell, but fatal errors or mistakes in `extension.js` will affect the stablity of the desktop. It also means you will be using the [Clutter](https://developer.gnome.org/clutter/stable/) and [St](https://developer.gnome.org/st/stable/) toolkits, although you may still use utility functions and classes from Gtk.

In `prefs.js` you **will not** have access to, or the ability to modify code running in GNOME Shell, however fatal errors or mistakes in `prefs.js` will be contained within that process and won't affect the stability of the desktop. In this process you will be using the [Gtk](https://developer.gnome.org/gtk3/stable/) toolkit, although you may also use Clutter as well via [Clutter-Gtk](https://developer.gnome.org/clutter-gtk/stable/).

You can open the preferences dialog for your extension manually with `gnome-extensions prefs`:

```sh
$ gnome-extensions prefs example@wiki.gnome.org
```

### stylesheet.css

`stylesheet.css` is CSS stylesheet which can apply custom styles to your St widgets in `extension.js` or GNOME Shell as a whole. For example, if you had the following widgets:

```js
// A standard StLabel
let label = new St.Label({
    text: 'LabelText',
    style_class: 'example-style'
});

// An StLabel subclass with `CssName` set to "ExampleLabel"
var ExampleLabel = GObject.registerClass({
    GTypeName: 'ExampleLabel',
    CssName: 'ExampleLabel'
}, class ExampleLabel extends St.Label {
});

let exampleLabel = new ExampleLabel({
    text: 'Label Text'
});
```

You could have this in your `stylesheet.css`:

```css
/* This will change the color of all StLabel elements */
StLabel {
    color: red;
}

/* This will change the color of all elements with the "example-style" class */
.example-style {
    color: green;
}

/* This will change the color of StLabel elements with the "example-style" class */
StLabel.example-style {
    color: blue;
}

/* This will change the color of your StLabel subclass with the custom CssName */
ExampleLabel {
    color: yellow;
}
```

## Imports & Modules

It's common for larger extensions or extensions with discrete components to separate code into modules. You can put code to be imported into `.js` files and import them in `extension.js`, `prefs.js` or each other.

```js
'use strict';

// Any imports this extension needs itself must also be imported here
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

// Mose importantly, variables declared with `let` or `const` are not exported
const LOCAL_CONSTANT = 42;
let localVariable = 'a value';

// This includes function expressions and classes declared with `class`
let _privateFunction = function() {};

// TIP: Private members are often prefixed with `_` in JavaScript, which is clue
// to other developers that these should only be used internally and may change
class _PrivateClass {
    constructor() {
        this._initted = true;
    }
}


// Function declarations WILL be available as properties of the module
function exportedFunction(a, b) {
    return a + b;
}

// Use `var` to assign any other members you want available as part the module
var EXPORTED_VARIABLE = 42;

var exportedFunction2 = function(...args) {
    return exportedFunction(...args);
}

var ExportedClass = class ExportedClass extends _PrivateClass {
    construct(params) {
        super();

        Object.assign(this, params);
    }
};
```

If placed in `example@wiki.gnome.org/exampleLib.js` the script above would be available as `Me.imports.extensionLib`. If it was in a subdirectory, such as `example@wiki.gnome.org/modules/exampleLib.js`, you would access it as `Me.imports.modules.exampleLib`.

```js
// GJS's Built-in Modules are in the top-level
// See: https://gitlab.gnome.org/GNOME/gjs/wikis/Modules
const Gettext = imports.gettext;
const Cairo = imports.cairo;

// GNOME APIs are under the `gi` namespace (except Cairo)
// See: http://devdocs.baznga.org/
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;

// GNOME Shell imports
const Main = imports.ui.main;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

// You can import your modules using the extension object we imported as `Me`.
const ExampleLib = Me.imports.exampleLib;

let myObject = new ExampleLib.ExportedClass();
ExampleLib.exportedFunction(0, ExampleLib.EXPORTED_VARIABLE);
```

Many of the elements in GNOME Shell like panel buttons, popup menus and notifications are built from reusable classes and functions. These common elements are the closest GNOME Shell has in terms of stable public API. Here are a few links to some commonly used modules.

 * https://gitlab.gnome.org/GNOME/gnome-shell/blob/master/js/misc/extensionUtils.js
 * https://gitlab.gnome.org/GNOME/gnome-shell/blob/master/js/ui/modalDialog.js
 * https://gitlab.gnome.org/GNOME/gnome-shell/blob/master/js/ui/panelMenu.js
 * https://gitlab.gnome.org/GNOME/gnome-shell/blob/master/js/ui/popupMenu.js

You can browse around in the `js/ui/` folder or any other JavaScript file under `js/` for more code to be reused. Notice the path structure in the links above and how they compare to the imports below:

```js
const ExtensionUtils = imports.misc.extensionUtils;
const ModalDialog = imports.ui.modalDialog;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
```

## Basic Debugging

> Some distributions may require you to be part of a `systemd` user group to access logs. On systems that are not using `systemd`, logs may be written to `~/.xsession-errors`.

Now is a good time to cover basic debugging and logging, which is an important part of developing any software. GJS has a number of built in global functions, although not all of them are useful for extensions.

### Logging

```js
// Log a string, usually to `journalctl`
log('a message');

// Log an Error() with a stack trace and optional prefix
try {
    throw new Error('An error occurred');
} catch (e) {
    logError(e, 'ExtensionError');
}

// Print a message to stdout
print('a message');

// Print a message to stderr
printerr('An error occured');
```

When writing extensions, `print()` and `printerr()` are not particularly useful since we won't have easy access to `gnome-shell`'s `stdin` and `stderr` pipes. You should generally use `log()` and `logError()` and watch the log in a new terminal with `journalctl`:

```sh
$ journalctl -f -o cat /usr/bin/gnome-shell
```

### GJS Console

Similar to Python, GJS also has a console you can use to test things out. However, you will not be able to access live code running in the `gnome-shell` process or import JS modules from GNOME Shell, since this a separate process.

```sh
$ gjs-console
gjs> log('a message');
Gjs-Message: 06:46:03.487: JS LOG: a message

gjs> try {
....     throw new Error('An error occurred');
.... } catch (e) {
....     logError(e, 'ConsoleError');
.... }

(gjs-console:9133): Gjs-WARNING **: 06:47:06.311: JS ERROR: ConsoleError: Error: An error occurred
@typein:2:16
@<stdin>:1:34
```

### Recovering from Fatal Errors

Despite the fact that extensions are written in JavaScript, the code is executed in the same process as `gnome-shell` so fatal programmer errors can crash GNOME Shell in a few situations. If your extension crashes GNOME Shell as a result of the `init()` or `enable()` hooks being called, this can leave you unable to log into GNOME Shell.

If you find yourself in this situation, you may be able to correct the problem from a TTY:

 1. Switch to a free TTY and log in

    You can do so, for example, by pressing `Ctrl` + `Alt` + `F4`. You may have to cycle through the `F#` keys.

 2. Start `journalctl` as above

    ```sh
    $ journalctl -f -o cat /usr/bin/gnome-shell
    ```

 3. Switch back to GDM and log in

    After your log in fails, switch back to the TTY running `journalctl` and see if you can determine the problem in your code. If you can, you may be able to correct the problem using `nano` or `vim` from the command-line.

If you fail to diagnose the problem, or you find it easier to review your code in a GUI editor, you can simply move your extension directory up one directory. This will prevent your extension from being loaded, without losing any of your code:

```sh
$ mv ~/.local/share/gnome-shell/extensions/example@wiki.gnome.org ~/.local/share/gnome-shell/
```

## Version Compatibility

> While writing your extension you will probably reference [GNOME Shell's JavaScript](https://gitlab.gnome.org/GNOME/gnome-shell/tree/master/js/) to reuse and modify code. On each GitLab page you can use the dropdown menu on the left-side to view a particular version or the "History" button on the right-side to view a list of changes.

In most programming languages there is a concept of "public" and "private" APIs. Public members are what programmers using library will use and remain stable, while private members are inaccessible and unstable which allows the library authors to make changes without breaking the public API. However, in JavaScript there is no reasonable way to make members inaccessible, or truly private.

In GNOME Shell there are a number of classes and functions that could be considered public and rarely change, but there's a good chance that you will want to modify private members anyways. One benefit of monkey-patching and using a prototyping language, is that it allows you to change low-level code and behaviour of GNOME Shell. The consequence is that when private or low-level code changes your extension may break.

To handle this, you can check the version of GNOME Shell that is running and adjust your code dynamically. This allows you to ensure your extension will work across versions when private members, or in rare cases public members, change the function signatures or class implementations.

```js
const GObject = imports.gi.GObject;

const Config = imports.misc.config;
const PanelMenu = imports.ui.panelMenu;

// Outputs a string like : "3.30.0"
log(Config.PACKAGE_VERSION);

// Getting the minor version as an integer (probably the most relevant)
let shellMinorVersion = parseInt(Config.PACKAGE_VERSION.split('.')[1]);

// Our PanelMenu.Button subclass
var ExampleIndicator = class ExampleIndicator extends PanelMenu.Button {
    _init(constructor_args) {
        // Chaining up to the super-class
        super._init(...constructor_args);

        // do some custom setup
    }

    customMethod() {
        // peform some custom operation
    }
}

// In gnome-shell >= 3.32 this class and several others became GObject
// subclasses. We can account for this change in a backwards-compatible way
// simply by re-wrapping our subclass in `GObject.registerClass()`
if (shellMinorVersion > 30) {
    ExampleIndicator = GObject.registerClass(
        {GTypeName: 'ExampleIndicator'},
        ExampleIndicator
    );
}
```

# Extension Creation

GNOME Shell ships with a program you can use to create a skeleton extension by running `gnome-extensions create`. Using `-i` it will walk you through picking a `name`, `description` and `uuid` before installing it to `~/.local/share/gnome-shell/extensions/` and opening `extension.js` in an editor.

For educational purposes we're going to build an extension from scratch instead. To get clean view of how your extension functions, you should restart GNOME Shell after making changes to the code. For this reason, most extension development happens in Xorg/X11 sessions rather than Wayland, which requires you to logout and login to restart .

To restart GNOME Shell in X11, pressing `Alt`+`F2` to open the *Run Dialog* and enter `restart` (or just `r`).

To run new extensions on Wayland you can run a nested gnome-shell using `dbus-run-session -- gnome-shell --nested --wayland`.

## Creating the Extension

Start by creating an extension directory, then open the two required files in `gedit` or another editor:

```sh
$ mkdir -p ~/.local/share/gnome-shell/extensions/example@wiki.gnome.org
$ cd ~/.local/share/gnome-shell/extensions/example@wiki.gnome.org
$ gedit extension.js metadata.json &
```

Populate `extension.js` and `metadata.json` with the basic requirements, remembering that `uuid` MUST match the directory name of your extension:

**metadata.json**
```js
{
    "uuid": "example@wiki.gnome.org",
    "name": "Example",
    "description": "This extension puts an icon in the panel with a simple dropdown menu.",
    "version": 1,
    "shell-version": [ "3.30", "3.32" ],
    "url": "https://gitlab.gnome.org/World/ShellExtensions/gnome-shell-extension-example"
}
```

**extension.js**
```js
'use strict';

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();


function init() {
    log(`initializing ${Me.metadata.name} version ${Me.metadata.version}`);
}


function enable() {
    log(`enabling ${Me.metadata.name} version ${Me.metadata.version}`);
}


function disable() {
    log(`disabling ${Me.metadata.name} version ${Me.metadata.version}`);
}
```

## Enabling the Extension

Firstly, we want to ensure we're watching the journal for any errors or mistakes we might have made. As described above, you can run this in a terminal to watch the output of GNOME Shell and extensions:

```sh
$ journalctl -f -o cat /usr/bin/gnome-shell
```

Next we'll enable the extension using `gnome-extensions enable`:

```sh
$ gnome-extensions enable example@wiki.gnome.org
```

Now restart GNOME Shell by pressing `Alt`+`F2` to open the *Run Dialog* and enter `restart` (or just `r`) and when it's finished you should see something like the following in the log:

```sh
GNOME Shell started at Sun Dec 23 2018 07:14:35 GMT-0800 (PST)
initializing Example version 1
enabling Example version 1
```

On wayland you can see the new extension running a nested gnome-shell `dbus-run-session -- gnome-shell --nested --wayland`.

## Adding UI Elements

Let's start by adding a button to the panel with a menu:

```js
'use strict';

const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const GObject = imports.gi.GObject;
const St = imports.gi.St;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;

// For compatibility checks, as described above
const Config = imports.misc.config;
const SHELL_MINOR = parseInt(Config.PACKAGE_VERSION.split('.')[1]);


// We'll extend the Button class from Panel Menu so we can do some setup in
// the init() function.
var ExampleIndicator = class ExampleIndicator extends PanelMenu.Button {

    _init() {
        super._init(0.0, `${Me.metadata.name} Indicator`, false);

        // Pick an icon
        let icon = new St.Icon({
            gicon: new Gio.ThemedIcon({name: 'face-laugh-symbolic'}),
            style_class: 'system-status-icon'
        });
        this.actor.add_child(icon);

        // Add a menu item
        this.menu.addAction('Menu Item', this.menuAction, null);
    }

    menuAction() {
        log('Menu item activated');
    }
}

// Compatibility with gnome-shell >= 3.32
if (SHELL_MINOR > 30) {
    ExampleIndicator = GObject.registerClass(
        {GTypeName: 'ExampleIndicator'},
        ExampleIndicator
    );
}

// We're going to declare `indicator` in the scope of the whole script so it can
// be accessed in both `enable()` and `disable()`
var indicator = null;


function init() {
    log(`initializing ${Me.metadata.name} version ${Me.metadata.version}`);
}


function enable() {
    log(`enabling ${Me.metadata.name} version ${Me.metadata.version}`);

    indicator = new ExampleIndicator();

    // The `main` import is an example of file that is mostly live instances of
    // objects, rather than reusable code. `Main.panel` is the actual panel you
    // see at the top of the screen.
    Main.panel.addToStatusArea(`${Me.metadata.name} Indicator`, indicator);
}


function disable() {
    log(`disabling ${Me.metadata.name} version ${Me.metadata.version}`);

    // REMINDER: It's required for extensions to clean up after themselves when
    // they are disabled. This is required for approval during review!
    if (indicator !== null) {
        indicator.destroy();
        indicator = null;
    }
}
```

Now save `extension.js` and restart GNOME Shell to see the button with it's menu in the panel.

## Modifying Behaviour

The most common way to modify the behaviour of GNOME Shell is to intercept or override signal handlers, which is the primary way events and user interaction are controlled. It's possible to modify the behaviour of elements like notifications, workspaces, the dash and almost any other element of GNOME Shell.

Describing the implementation of any particular element is out of the scope of this guide, so as a simple example we'll steal the `scroll-event` from the volume indicator for our panel button:

```js
// Grab the Volume Indicator from the User Menu
const VolumeIndicator = Main.panel.statusArea.aggregateMenu._volume;


var ExampleIndicator = class ExampleIndicator extends PanelMenu.Button {

    _init() {
        super._init(0.0, `${Me.metadata.name} Indicator`, false);

        let icon = new St.Icon({
            gicon: new Gio.ThemedIcon({name: 'face-laugh-symbolic'}),
            style_class: 'system-status-icon'
        });
        this.actor.add_child(icon);

        this.menu.addAction('Menu Item', this.menuAction, null);

        // Prevent the volume indicator from emitting the ::scroll-event signal
        VolumeIndicator.reactive = false;

        // Connect the callback to our button's signal
        //
        // NOTE: use `Function.bind()` to bind the scope (ie. `this`) to the
        // proper context. In this example, when `_onScrollEvent()` is invoked
        // `this` will be the `VolumeIndicator` instance the function expects.
        this._onScrollEventId = this.actor.connect(
          'scroll-event',
          VolumeIndicator._onScrollEvent.bind(VolumeIndicator)
        );
    }

    menuAction() {
        log('Menu item activated');
    }

    destroy() {
        // Disconnect from the scroll-event signal
        //
        // NOTE: When we called `GObject.connect()` above, it returned an
        // integer for the connection handler. We saved that so that we can use
        // it now to disconnect the callback.
        this.actor.disconnect(this._onScrollEventId);

        // Reset the volume indicator reactivity
        VolumeIndicator.reactive = true;

        super.destroy();
    }
}
```

Now save `extension.js` and restart GNOME Shell to test the volume control.


## Modifying the UI

Since we are working within an active process, we can also modify the properties of existing elements in the UI. Let's expand the button's menu to include some other elements from the panel:

```js
var ExampleIndicator = class ExampleIndicator extends PanelMenu.Button {

    _init() {
        super._init(0.0, `${Me.metadata.name} Indicator`, false);

        // Pick an icon
        let icon = new St.Icon({
            gicon: new Gio.ThemedIcon({name: 'face-laugh-symbolic'}),
            style_class: 'system-status-icon'
        });
        this.actor.add_child(icon);

        // Keep a record of the original visibility of each panel item
        this.states = {};

        // Add a menu item for each item in the panel
        for (let name in Main.panel.statusArea) {
            // Remember this item's original visibility
            this.states[name] = Main.panel.statusArea[name].actor.visible;

            this.menu.addAction(
                `Toggle "${name}"`,
                this.togglePanelItem.bind(null, name),
                null
            );
        }
    }

    /**
     * togglePanelItem:
     * @param {string} name - name of the panel item
     *
     * Don't be a jerk to your future self; document your code!
     */
    togglePanelItem(name) {
        log(`${name} menu item activated`);

        try {
            let statusItem = Main.panel.statusArea[name];

            // Many classes in GNOME Shell are actually native classes (non-GObject)
            // with a ClutterActor (GObject) as the property `actor`. St is an
            // extension of Clutter so these may also be StWidgets.
            statusItem.actor.visible = !statusItem.actor.visible;
        } catch (e) {
            logError(e, 'togglePanelItem');
        }
    }

    // We'll override the destroy() function to revert any changes we make
    destroy() {
        // Restore the visibility of the panel items
        for (let [name, visibility] of Object.entries(this.states)) {
            Main.panel.statusArea[name].actor.visible = visibility;
        }

        // Chain-up to the super-class after we've done our own cleanup
        super.destroy();
    }
}
```

Now save `extension.js` and restart GNOME Shell again to see the menu allowing you toggle the visibility of panel items.

# Extension Preferences

Our preferences dialog will be written in Gtk, which gives us a lot of options for how we present settings to the user. You may consider looking through the GNOME Human Interface Guidelines for ideas or guidance. Keep in mind these are only guidelines and you *should* depart from them when necessary to make the most intuitive interface you can.

## GSettings

> **Programmer errors with GSettings are fatal in *all* languages and will cause the application to crash!**
>
> Normally this means your application will quit and fail to start until you correct the problem. Since your extension is part of the `gnome-shell` process, this can prevent you from logging in. See [Recovering from Fatal Errors](#Recovering_from_Fatal_Errors) above.

The first thing to do is create a subdirectory for your settings schema and an empty schema file:

```sh
$ mkdir schemas/
$ gedit schemas/org.gnome.shell.extensions.example.gschema.xml
```

Then open the file in your text editor and create a schema describing the settings for your extension:

```sh#!highlight xml
<?xml version="1.0" encoding="UTF-8"?>
<schemalist>
  <schema id="org.gnome.shell.extensions.example" path="/org/gnome/shell/extensions/example/">
    <!-- The key type "a{sb}" describes a dictionary of string-boolean pairs -->
    <!-- See also: https://developer.gnome.org/glib/stable/gvariant-format-strings.html -->
    <key name="panel-states" type="a{sb}">
      <default>{}</default>
    </key>
    <key name="show-indicator" type="b">
      <default>true</default>
    </key>
  </schema>
</schemalist>
```

Generally, you are advised not to use "GNOME" in your application's name or ID to avoid implying that your application is officially endorsed by the GNOME Foundation. In the case of GSchema IDs however, it is convention to use the above `id` and `path` form so that all GSettings for extensions can be found in a common place.

Once you are done defining you schema, save the file and compile it into it's binary form:

```sh
$ glib-compile-schemas schemas/
$ ls schemas
example.gschema.xml  gschemas.compiled
```

Now that our GSettings schema is compiled and ready to be used, we'll integrate it into our extension. We'll return to the example above that let's us change the visibility of panel items:

```js
var ExampleIndicator = class ExampleIndicator extends PanelMenu.Button {

    _init() {
        super._init(0.0, `${Me.metadata.name} Indicator`, false);

        // Get the GSchema source so we can lookup our settings
        let gschema = Gio.SettingsSchemaSource.new_from_directory(
            Me.dir.get_child('schemas').get_path(),
            Gio.SettingsSchemaSource.get_default(),
            false
        );

        this.settings = new Gio.Settings({
            settings_schema: gschema.lookup('org.gnome.shell.extensions.example', true)
        });

        // Bind our indicator visibility to the GSettings value
        //
        // NOTE: Binding properties only works with GProperties (properties
        // registered on a GObject class), not native JavaScript properties
        this.settings.bind(
            'show-indicator',
            this.actor,
            'visible',
            Gio.SettingsBindFlags.DEFAULT
        );

        // Watch the settings for changes
        this._onPanelStatesChangedId = this.settings.connect(
            'changed::panel-states',
            this._onPanelStatesChanged.bind(this)
        );

        // Keep record of the original state of each item
        this.states = {};

        // Read the saved states
        let variant = this.settings.get_value('panel-states');

        // Unpack the GSettings GVariant
        //
        // NOTE: `GSettings.get_value()` returns a GVariant, which is a
        // multi-type container for packed values. GJS has two helper functions:
        //
        //  * `GVariant.unpack()`
        //     This function will do a shallow unpacking of any variant type,
        //     useful for simple types like "s" (string) or "u" (uint32/Number).
        //
        //  * `GVariant.deep_unpack()`
        //     A deep, but non-recursive unpacking, such that our variant type
        //     "a{sb}" will be unpacked to a JS Object of `{ string: boolean }`.
        //     `GVariant.unpack()` would return `{ string: GVariant }`.
        this.saved = variant.deep_unpack();

        // Pick an icon
        let icon = new St.Icon({
            gicon: new Gio.ThemedIcon({name: 'face-laugh-symbolic'}),
            style_class: 'system-status-icon'
        });
        this.actor.add_child(icon);

        // Add a menu item for each item in the panel
        for (let name in Main.panel.statusArea) {
            // Record this item's original visibility
            this.states[name] = Main.panel.statusArea[name].actor.visible;

            // Restore our settings
            if (name in this.saved) {
                log(`Restoring state of ${name}`);
                Main.panel.statusArea[name].actor.visible = this.saved[name];
            }

            this.menu.addAction(
                `Toggle "${name}"`,
                this.togglePanelItem.bind(this, name),
                null
            );
        }
    }

    _onPanelStatesChanged(settings, key) {
        // Read the new settings
        this.saved = this.settings.get_value('panel-states').deep_unpack();

        // Restore or reset the panel items
        for (let name in this.states) {
            // If we have a saved state, set that
            if (name in this.saved) {
                Main.panel.statusArea[name].actor.visible = this.saved[name];

            // Otherwise restore the original state
            } else {
                Main.panel.statusArea[name].actor.visible = this.states[name];
            }
        }
    }

    togglePanelItem(name) {
        log(`${name} menu item activated`);

        let statusItem = Main.panel.statusArea[name];
        statusItem.actor.visible = !statusItem.actor.visible;

        // Store our saved state
        this.saved[name] = statusItem.actor.visible;
    }

    destroy() {
        // Stop watching the settings for changes
        this.settings.disconnect(this._onSettingsChangedId);

        // Store the panel settings in GSettings
        this.settings.set_value(
            'panel-states',
            new GLib.Variant('a{sb}', this.saved)
        );

        // Restore the visibility of the panel items
        for (let [name, visibility] of Object.entries(this.states)) {
            Main.panel.statusArea[name].actor.visible = visibility;
        }

        super.destroy();
    }
}
```

Now save `extension.js` and restart GNOME Shell again. Change the visibility of one of the panel items and then disable the extension:

```sh
$ gnome-extensions disable example@wiki.gnome.org
```

The panel button from the extension should disappear, and the hidden panel item should reappear. Renable the extension and your settings should be restored:

```sh
$ gnome-extensions enable example@wiki.gnome.org
```

## Preferences Window

Now that we have GSettings for our extension, we will give the use some control by creating a simple preference dialog. Start by creating the `prefs.js` file and opening it in your text editor:

```sh
$ gedit prefs.js
```

Then we'll create a simple grid with a title, label and button for resetting our saved settings:

```js
'use strict';

const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();


function init() {
}

function buildPrefsWidget() {

    // Copy the same GSettings code from `extension.js`
    let gschema = Gio.SettingsSchemaSource.new_from_directory(
        Me.dir.get_child('schemas').get_path(),
        Gio.SettingsSchemaSource.get_default(),
        false
    );

    this.settings = new Gio.Settings({
        settings_schema: gschema.lookup('org.gnome.shell.extensions.example', true)
    });

    // Create a parent widget that we'll return from this function
    let prefsWidget = new Gtk.Grid({
        margin: 18,
        column_spacing: 12,
        row_spacing: 12,
        visible: true
    });

    // Add a simple title and add it to the prefsWidget
    let title = new Gtk.Label({
        // As described in "Extension Translations", the following template
        // lit
        // prefs.js:88: warning: RegExp literal terminated too early
        //label: `<b>${Me.metadata.name} Extension Preferences</b>`,
        label: '<b>' + Me.metadata.name + ' Extension Preferences</b>',
        halign: Gtk.Align.START,
        use_markup: true,
        visible: true
    });
    prefsWidget.attach(title, 0, 0, 2, 1);

    // Create a label to describe our button and add it to the prefsWidget
    let buttonLabel = new Gtk.Label({
        label: 'Reset Panel Items:',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(buttonLabel, 0, 1, 1, 1);

    // Create a 'Reset' button and add it to the prefsWidget
    let button = new Gtk.Button({
        label: 'Reset Panel',
        visible: true
    });
    prefsWidget.attach(button, 1, 1, 1, 1);

    // Connect the ::clicked signal to reset the stored settings
    button.connect('clicked', (button) => this.settings.reset('panel-states'));

    // Create a label & switch for `show-indicator`
    let toggleLabel = new Gtk.Label({
        label: 'Show Extension Indicator:',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(toggleLabel, 0, 2, 1, 1);

    let toggle = new Gtk.Switch({
        active: this.settings.get_boolean ('show-indicator'),
        halign: Gtk.Align.END,
        visible: true
    });
    prefsWidget.attach(toggle, 1, 2, 1, 1);

    // Bind the switch to the `show-indicator` key
    this.settings.bind(
        'show-indicator',
        toggle,
        'active',
        Gio.SettingsBindFlags.DEFAULT
    );

    // Return our widget which will be added to the window
    return prefsWidget;
}
```

To test the new preferences dialog, you can launch it directly from the command line:

```sh
$ gnome-extensions prefs example@wiki.gnome.org
```

The extension should be kept up to date with any changes that happen, because of the signal handler in `extension.js` watching for changes.

# Extension Translations

Explaining how Gettext translations work in detail is out of the scope of this document, however we'll cover some basics and how it's used in GJS and GNOME Shell extensions. More information and guides to translating can be found on the [GNOME Translation Project](https://wiki.gnome.org/TranslationProject) wiki.

## Gettext Module

GJS includes a built-in module for Gettext, which is typically initialized like this:

```js
const Gettext = imports.gettext;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

// Bind gettext to your extension's "domain". This is generally a short, unique
// string like a package name. You could use your extension UUID.
Gettext.textdomain('example');

// Bind the text domain to the translations in your `locale/` directory
Gettext.bindtextdomain('example', Me.dir.get_child('locale').get_path());

// The most common function is `gettext()`, by convention aliased to `_()`.
const _ = Gettext.gettext;
```

Once initialized, you use Gettext's functions to mark your strings as translatable:

```js

// This function marks a string to be translated while parsing with gettext and
// returns the correct translation as a string during execution.
let singular = new St.Label({
    text: _('Example')
});

// The other function you are likely to use is `ngettext()` which is used for
// pluralized translations.
let amount = 2;

let plural = new St.Label({
    text: Gettext.ngettext('%d Example', '%d Examples', amount).format(amount)
});
```


## Preparing Translations

The more complex your extension the more likely you are to use a build system that includes conveniences for Gettext and translations. For educational purposes we'll go through some simple examples that will probably work for most extensions. Start by creating a `po` directory for the template and raw translations as well as a `locale` directory for the compiled translations:

```sh
$ mkdir ~/.local/share/gnome-shell/extensions/example@wiki.gnome.org/po/
$ mkdir ~/.local/share/gnome-shell/extensions/example@wiki.gnome.org/locale/
```

### POT File

The `pot` file is generated by `xgettext` by scanning your source code for strings marked as translatable. This is the template translators will use to create translations for their language.

```sh
$ ls
extension.js  locale  metadata.json  po  prefs.js  schemas  stylesheet.css
$ xgettext --from-code=UTF-8 --output=po/example.pot *.js
```

Running `xgettext` on the example code above would generate the template file below:

```sh#!highlight po
# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-08 21:03-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: test.js:21
msgid "Example"
msgstr ""

#: test.js:29
#, javascript-format
msgid "%d Example"
msgid_plural "%d Examples"
msgstr[0] ""
msgstr[1] ""
```

### PO Files

Each `.po` file contains the strings of `messages.pot` translated into a particular language. Generally, you will make your POT file available and translators will submit the translated `.po` file for their language. These can be created easily with a program like [Poedit](https://wiki.gnome.org/Apps/Gtranslator|GTranslator]] or [[https://poedit.net/) and submitted by Pull/Merge Request.

As a simple example, the following will create a near duplicate of the above. The translator takes this file and fills in each `msgstr` with the French translation of the `msgid`:

```sh
$ msginit --no-translator \
          --input=po/example.pot \
          --output-file=po/fr.po --locale=fr
$ ls po/
example.pot  fr.po
```

### MO Files

The `.pot` and `.po` files are the "source code" of translations and generally not distributed with a packed extension. Instead, these files are compiled into `.mo` files which Gettext will use to fetch translated strings for different languages.

```sh
$ mkdir -p locale/fr/LC_MESSAGES/
$ msgfmt po/fr.po -o locale/fr/LC_MESSAGES/example.mo
$ ls locale/fr/LC_MESSAGES/
example.mo
```

## Gettext and JavaScript Template Literals

It is worth explaining why we use `String.prototype.format()` function and not JavaScript's template literals with translatable strings. Due to an [open bug in Gettext](https://savannah.gnu.org/bugs/?50920), JavaScript template literals are not yet supported in Gettext.

In fact, slashes in template literals *anywhere* in your file can break Gettext when parsing with `xgettext`:

```js
// This may cause strange problems when `xgettext` is parsing the file
// eg. "foo.js:5: warning: RegExp literal terminated too early"
let path = `${directory}/${filename}.${extension}`;

// Instead, use string concatenation...
let path = directory + '/' + filename + '.' + extension;

// ...or GLib.build_filenamev()
const GLib = imports.gi.GLib;

let path = GLib.build_filenamev([directory, `${filename}.${extension}`]);
```

GJS ships with a [format](https://gitlab.gnome.org/GNOME/gjs/blob/master/doc/Modules.md#format) module (`imports.format`) which has three functions: `vprintf()`, `printf()` and `format()`. In GNOME Shell `format()` is applied to the `String` prototype, so that it can be used like so:

```js
// If you're using GJS in a standalone application, you should do this yourself
String.prototype.format = imports.format.format;

// Two examples that both output: A string with a digit "5" and and string "test"
let num = 5;
let str = 'test';

// With String.prototype.format()
'A string with a digit "%d" and string "%s"'.format(num, str);

// With native template literals
'A string with a digit "${num}" and string "${str}"';
```


# Extension Utils

Long ago, Giovanni Campagna (aka gcampax) wrote a small helper script for Gettext translations and GSettings called `convenience.js`. This script was used so widely by extension authors that they were merged in GNOME Shell in version 3.32.

## initTranslations()

`initTranslations()` is a simple helper that will set the text domain and bind it to the `locale/` directory in your extension's folder. It therefore expects your extension to have it's translations in this directory. If this directory does not exist, it will assume your extension has been installed as a system extension and bind the text domain to the system locale directory.

```js
'use strict';

const Gettext = imports.gettext;

const ExtensionUtils = imports.misc.extensionUtils;

// ...code for `extension.js` or `prefs.js`...

function init() {
    // If `gettext-domain` is defined in `metadata.json` you can simply call
    // this function without an argument.
    ExtensionUtils.initTranslations();

    // Otherwise, you should call it with your "domain" as described above
    ExtensionUtils.initTranslations('example');
}
```

## getSettings()

`getSettings()` is another simple helper function that will create a GSettingsSchemaSource and use it to lookup settings schema before trying to create a GSettings object for it, thus avoiding a possible crash. This function expects that your schema is compiled and in the `schemas/` directory in your extension's folder. If this directory does not exist, it will assume your extension has been installed as a system extension and use the default system source to try and lookup your settings.

```js
const ExtensionUtils = imports.misc.extensionUtils;

// If `settings-schema` is defined in `metadata.json` you can simply call this
// this function without an argument.
let settings = ExtensionUtils.getSettings();

// Otherwise, you should call it with your GSchema ID as described above
let settings = ExtensionUtils.getSettings('org.gnome.shell.extensions.example');

// You can also use this function to safely create a GSettings object for other
// schemas without knowing whether they exist or not.
let nautilusSettings = null;

try {
    nautilusSettings = ExtensionUtils.getSettings('org.gnome.nautilus');
} catch (e) {
    logError(e, 'Failed to load Nautilus settings');
}
```


# Extension Distribution

As mentioned earlier, wherever you get extensions they are distributed as Zip files.

## Creating a Zip file

Simply pack up the *contents* of your extension directory, excluding any files that your extension doesn't need to run:

```sh
$ zip -r example@wiki.gnome.org.zip . --exclude=po/\*
  adding: extension.js (deflated 66%)
  adding: prefs.js (deflated 65%)
  adding: stylesheet.css (deflated 63%)
  adding: metadata.json (deflated 37%)
  adding: schemas/ (stored 0%)
  adding: schemas/org.gnome.shell.extensions.example.gschema.xml (deflated 44%)
  adding: schemas/gschemas.compiled (deflated 36%)
  adding: locale/ (stored 0%)
  adding: locale/fr/ (stored 0%)
  adding: locale/fr/LC_MESSAGES/ (stored 0%)
  adding: locale/fr/LC_MESSAGES/example.mo (deflated 25%)
```

## Submitting to extensions.gnome.org

GNOME Shell extensions are patches applied during runtime that can affect the stability and security of the desktop. For this reason all extensions submitted for distribution from https://extensions.gnome.org/ are thoroughly reviewed. The requirements and tips for getting you extension approved are described on the [Review](https://wiki.gnome.org/Projects/GnomeShell/Extensions/Review) page.

Once you have reviewed the requirements for approval and your extension is zipped, visit https://extensions.gnome.org/upload/ to submit it for review.

